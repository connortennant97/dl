

Transfer Learning to classify playing cards using VGG-16. By default the VGG-16 model focuses on categorising an image into one of 1000 categories with a wide range of possible categories. For this project VGG-16 was used to extract features from an image to then be used by a 2D CNN to attempt to predict the cards suit [Clubs, Diamonds, Hearts, Spades] 

For this the included VGG-16 model provided with TensorFlow was used that has been pretrained using the ImageNet dataset containing millions of images, luckily meaning we wont have to assemble as large of a dataset as that in order to train the model.

The data set used came from kaggle (https://www.kaggle.com/gunhcolab/object-detection-dataset-standard-52card-deck), this dataset contains around 600 images of playing cards (40 MB) a subset of the data was split between training & test sets (108, 28), 27 of each suits for training and 7 for testing the files were organised into a folder structure shown below:



clubs

  >img

  >img

  >...

diamonds

  >img

  >img

  >...

hearts

  >img

  >img

  >...

spades
  >img

  >img

  >...



While processing the dataset into the above format it was noted that while both sets contained 52 categories as we would expect from a standard deck of paying cards, the data contained a label of "seven of seven" which refered to a "seven of clubs" image so after removing that we were still missing a category label in the training set.

The missing label was the "six of clubs" after changing the lavel in the datasets excel spreadsheet that points to each image by filename and shows the categories along with additional data we will not be using for this application like the locations of the edges of the cards for object detection applications.
In the test label set there was no instance of the "four of clubs" or "the three of hearts" labels so images had to be added manually for these labels, the labels csv also contained multiple mispellings of "diamonds" as "dimaonds" so they also had to be ammended also before we were able to train the model, in the case of the "five of diamonds both spelling were present in the test label set; luckily this error wasn't present in the training set. 

The test label set was also missing an entry of the "seven of diamonds" so an unused image had to be carefully picked from the training set which was not already in our training data sub set (d72.jpg was inserted into the created test data sub set). c48.jpg & c49.jpg for missing "four of clubs" and h33.jp & h36.jpg were used for the missing "three of hearts"

After making the above ammendments to the training and test folders created the preprocessing of the data set is complete; In summary we have arranged images into the training and test sets with the images categorised by type of card using folder names, which will allow the data to be more easily consuimed by tensorflow by allowing the labels to be inferred from the file structure. Both of the created sets contain 52 folder as we would expect with the training data containing 5 images per folder for a total of 260 images, the test set is smaller with only 84 images, some test labels contained 2 images so is larger than 52.
"""



"""# upload data

Upload the data.zip file containing the training & testing image sets in the 'cards_suits_train', 'cards_suits_test' folders

# pretrained VGG19 model
"""

# import required librarys
#from keras.applications.vgg16 import VGG16, preprocess_input
from keras.applications.vgg16 import VGG16, preprocess_input
from keras.backend import dropout, int_shape
from keras.preprocessing.image import load_img
import matplotlib.pyplot as plt
import tensorflow as tf
from keras import activations, models
import os
import random
# https://www.kaggle.com/hugopaigneau/playing-cards-dataset
# trained using 7 images of each card from above dataset

base_model_str = 'vgg16'

input_shape = (224, 224, 3)

train_batch_size = 16
test_batch_size = 16

initial_epochs = 10
tuning_epochs = 40

initial_learning_rate = 0.0001
tuning_learning_rate = 0.000001



save_ckp_step = 5
tuning_n_layers_freeze = 13
seed = 1212224441

from numpy.random import seed
seed(1212224441)

tf.random.set_seed(1212224441)


def plot_loss_acc(history, loss_ylim, **kwargs):
  #show_graph(acc, loss)

  acc = history.history['accuracy']
  val_acc = history.history['val_accuracy']

  #loss = [v/100 for v in history.history['loss']]
  #val_loss = [v/100 for v in history.history['val_loss']]
  
  loss = history.history['loss']
  val_loss = history.history['val_loss']
  plt.figure(figsize=(initial_epochs, 12))
  plt.subplot(2, 1, 1)

  plt.plot(acc, label='Training Accuracy')
  plt.plot(val_acc, label='Testing Accuracy')
  if kwargs.get('tuning_history'):

        plt.plot(acc + kwargs.get('tuning_history').history['accuracy'], '--', label='Training Accuracy (tuning)')
        plt.plot(val_acc + kwargs.get('tuning_history').history['val_accuracy'], '--', label='Testing Accuracy (tuning)')

  plt.legend(loc='lower right')
  plt.ylabel('Accuracy')
  plt.ylim([0,1])
  plt.title('Training and Testing Accuracy')

  plt.subplot(2, 1, 2)

  plt.plot(loss, label='Training Loss')
  plt.plot(val_loss, label='Testing Loss')
  if kwargs.get('tuning_history'):
    plt.plot(loss + kwargs.get('tuning_history').history['loss'], '--', label='Training Loss (tuning)')
    plt.plot(val_loss + kwargs.get('tuning_history').history['val_loss'], '--', label='Testing Loss (tuning)')


  plt.legend(loc='upper right')
  plt.ylabel('Cross Entropy')
  plt.ylim([0, 5])
  #plt.ylim([0, 1])
  plt.title('Training and Testing Loss')
  plt.xlabel('epoch')
  plt.show()

  if kwargs.get('save_path'):
    try:
      plt.savefig(kwargs.get('save_path'))
    except:
      print('failed to save plot: path=',kwargs.get('save_path'))


# import pre-trained VGG-16 model for feature extraction, include_top=False remove the pre-trained classification layer as its not required
base_model = VGG16(include_top=False, classes=4, weights='imagenet', input_shape=input_shape)
# freeze first 10 layers so as to not affect feature extraction but allow training of more task specific layers

base_model.trainable = False

"""After intitialising the base VGG-16 model we need to load our data, for this the data_suits.zip file needs to be uploaded to colab using the upload file fucntion in the top left. After uploading run the code below to extract the training, testing datasets"""



"""The folders containing the daatsets should now be shown in the colab local files so now we can load them for use by tensorflow"""

train_data = tf.keras.utils.image_dataset_from_directory("./data/cards_suits_train", labels="inferred",batch_size=train_batch_size, label_mode="int", color_mode="rgb", image_size=input_shape[:2], crop_to_aspect_ratio=True)
test_data = tf.keras.utils.image_dataset_from_directory("./data/cards_suits_test", labels="inferred", batch_size=test_batch_size, label_mode="int", color_mode="rgb", image_size=input_shape[:2], crop_to_aspect_ratio=True)
myCards = tf.keras.utils.image_dataset_from_directory("./data/mydata", labels="inferred", batch_size=test_batch_size, label_mode="int", color_mode="rgb", image_size=input_shape[:2], crop_to_aspect_ratio=True)

resize_and_rescale = tf.keras.Sequential([

])

train_data.shuffle(3, reshuffle_each_iteration=True, seed=1441)
test_data.shuffle(3, reshuffle_each_iteration=True, seed=1212)
myCards.shuffle(3, reshuffle_each_iteration=True, seed=122)
"""Due to the limited size of the training data, data augmentation is used to help create a more diverse set of data for training.
This is simply done by randomly fliping the image horizontaly alon with applying slight rotations to the image
"""
random.seed(1234)
data_preprocess_augmentation = tf.keras.Sequential([


  tf.keras.layers.Resizing(224,224),
  tf.keras.layers.Rescaling(1./255),
  tf.keras.layers.RandomRotation(0.9, seed=222),
  tf.keras.layers.RandomContrast(0.9, seed=555)
  
])


top_layer = tf.keras.layers.Dense(4, activation='softmax')

print('Number of training batches: %d' % tf.data.experimental.cardinality(train_data))

print('Number of test batches: %d' % tf.data.experimental.cardinality(test_data))

#build model

inputs = tf.keras.Input(shape=input_shape)
aug_inputs = data_preprocess_augmentation(inputs)
x = base_model(aug_inputs)
x = tf.keras.layers.Flatten()(x)
x = tf.keras.layers.Dense(16)(x)
outputs = tf.keras.layers.Dense(4, activation='softmax')(x)
model = tf.keras.Model(inputs, outputs)


model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=initial_learning_rate),
              loss=tf.keras.losses.SparseCategoricalCrossentropy(),
              metrics=['accuracy'])

model.summary()


loss0, accuracy0 = model.evaluate(test_data)

print("initial loss test: {:.2f}".format(loss0))
print("initial accuracy test: {:.2f}".format(accuracy0))

loss0, accuracy0 = model.evaluate(myCards)

print("initial loss test: {:.2f}".format(loss0))
print("initial accuracy test: {:.2f}".format(accuracy0))


# Include the epoch in the file name (uses `str.format`)
checkpoint_path = "./checkpoints/train_cp-{epoch:04d}.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

# Create a callback that saves the model's weights every 5 epochs
cp_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path, 
    verbose=1, 
    save_weights_only=True,
    save_freq=800)


history = model.fit(train_data,
                    epochs=initial_epochs,
                    validation_data=test_data,
                    )


model.save_weights("./weights/weights_20")

plot_loss_acc(history, 20)

# rebuild model
#set first 10 layers as untrainable and train the rest

tuning_model = model
tuning_model.summary()

tuning_model.trainable = True

for layer in tuning_model.get_layer(base_model_str).layers[:tuning_n_layers_freeze]:
  layer.trainable = False
  

tuning_model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=tuning_learning_rate),
              loss=tf.keras.losses.SparseCategoricalCrossentropy(),
              metrics=['accuracy'])

tuning_model.summary()

"""# New section"""


loss0, accuracy0 = tuning_model.evaluate(test_data)

print("initial classifier training loss: {:.2f}".format(loss0))
print("initial classifier training accuracy: {:.2f}".format(accuracy0))

loss0, accuracy0 = model.evaluate(myCards)

print("initial classifier training loss validation: {:.2f}".format(loss0))
print("initial classifier training accuracy validation: {:.2f}".format(accuracy0))

# Include the epoch in the file name (uses `str.format`)
checkpoint_path = "/content/gdrive/MyDrive/checkpoints/tuning_cp-{epoch:04d}.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)


# Create a callback that saves the model's weights every 5 epochs
cp_callback_tuning = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path, 
    verbose=1, 
    save_weights_only=True,
    step=save_ckp_step)



history_tuning = tuning_model.fit(train_data,
                    epochs=tuning_epochs,
                    validation_data=test_data,
                    )

for i in range(10):
  loss0, accuracy0 = model.evaluate(test_data)

  print("final loss test: {:.2f}".format(loss0))
  print("final accuracy test: {:.2f}".format(accuracy0))

  loss0, accuracy0 = model.evaluate(myCards)

  print("final loss validation: {:.2f}".format(loss0))
  print("final accuracy validation: {:.2f}".format(accuracy0))


plot_loss_acc(history_tuning, 2)

plot_loss_acc(history, 20, tuning_history=history_tuning)
